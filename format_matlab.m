function res = format_matlab(varargin)
    %format_matlab(fromfile, tofile, settings)
    %  Beautify matlab
    %  - Add spaces around operators
    %  - put commands on separate lines
    %  - remove spaces from anonymous functions for readability (or leave
    %    unchanged)
    %  - add "end" to oldstyle functions
    %  - remove unnecessary semicolons
    %  Much effort is put in keeping the meaning of the code always unchanged.
    %  For instance it detects command style statements and leave them unchanged
    %  (as spaces are critical there)
    %
    %  Usage:
    %    format_matlab - if no arguments are given, a simple dialog box is
    %           plotted.
    %    format_matlab -c - the current editor page is formatted.
    %    format_matlab afile - if no output file is given, the new text is 
    %           only printed
    %    format_matlab fromfile tofile - changes written to tofile (can be
    %           the same name)
    %    format_matlab('fromfile','tofile',sett) - use settings as
    %           specified in the structure sett (see below)
    %    format_matlab -add - add shortcuts to favorites (works only in
    %           MATLAB versions >=R2018a)
    %    sett=format_matlab('-s'); get a structure with the current default
    %           settings
    %    format_matlab('-s',struct('ignore_anonymous_fun',false)) - change
    %           the default settings (only the fields that deviate from the
    %           default settings need to be defined)
    %    format_matlab('-s',[]) reset the default settings to the preset
    %           ones
    %    format_matlab -d dir - inplace formatting of all files in
    %           directory.
    %    format_matlab -t dir - test formatting of all files in dir.
    %           If your matlab version supports it, you can use **\*.m
    %           to scan subdirectories too. In the test is included a
    %           comparison of the warnings with the matlab command
    %           "checkcode". The changes in warnings are showed on the
    %           screen.
    %
    %  Settings stucture:
    %    Fields:
    %    exception_anonymous_fun - logical set true if you don't want to
    %                           format anonymous functions
    %    remove_space_anonymous_fun - logical set true if you want to
    %                           remove spaces from anonymous functions
    %    remove_double_space  - logical, remove double spaces? (except
    %                           indent)
    %    linebreak_after_semicolon - logical, make new lines after ';' and ','
    %    remove_unneeded_spaces- logical, remove first all uneccesary spaces
    %                           (run only once with true, otherwise slow!)
    %    addspace_before_comment- logical, add one space before each
    %                           comment
    %    addspace_before        - cell list of opers that should get a
    %                           space before them (for instance '~')
    %    addspace_around_no_unary - cell list of special opers that can be
    %                           unary or not ('+' and '-'}. Depending on
    %                           their role they get spaces around or spaces
    %                           after, note that in somme cases the spaces
    %                           determine the role [0 -1] or [0 - 1]
    %    addspace_after       - cell, list of opers that should have space
    %                           after them (for instance {';'}
    %    addspace_around      - cell, list of opers that should have spaces
    %                           at both sides (for instance {'*'})
    %    remove_semicolons    - logical remove unneccesary semicolons after
    %                           'end', 'function', 'if' etc
    %    remove_semicolons_keywords - cell list of keywords to remove
    %                           semicolons
    %    replace_tabs_w_space - replace tabs with space (necessary for
    %                           correct indentation)
    %    update_indent        - logical if true update teh indentation
    %    indent_keywords      - structure with keywords and indentation rules
    %    indent_line_extension - integer number of extra spaces in multi-line statements
    %    FunctionIndentingFormat - kind of behaviour with functions (see MATLAB settings:
    %                           ClassicFunctionIndent or MixedFunctionIndent or AllFunctionIndent
    %    add_end_to_oldstylefunctions - logical if set to true all functions get a trailing end
    %
    %
    %  Directives in text:
    %    %#format<off> stop formatting from this line on
    %    %#format<on> start formatting again from this line on
    %
    %  Known issues:
    %    (*) After splitting a line, the warning suppression directives are put
    %        on the last line. This will often be the wrong line. For instance:
    %
    %        for i=1:n,a=[a i];end; %#ok<AGROW>
    %
    %        becomes:
    %        for i = 1:n
    %           a = [a i];
    %        end %#ok<AGROW>
    %
    %        This will generate 2 warnings: 1 the formerly suppressed warning and a
    %        warning that the suppression of the last line is no longer needed.
    %        Solution: move the directive to the correct place(s) afterwards
    %
    %
    %    (*) Statements after a if/while/elseif condition delimited only with space are
    %        not fully supported. The following will be wrongly indended and a warning is given:
    %        if cond if cond1
    %           test,end
    %        end
    %
    %        Solution: add comma or semicolon
    %        if cond, if cond1
    %           test,end
    %        end
    %
    %
    
    %   Egbert van Nes, WUR
    %change code here to adapt the default settings
    if nargin == 0
        id = listdlg('promptstring', 'What do you want to do?', 'SelectionMode', 'single', 'ListString', {'Format current editor page', 'Format current directory','Format file/directory', 'Change settings', 'Add shortcuts'});
        switch id
            case 1
                varargin = {'-c'};
            case 2
                varargin = {'-d'};
            case 3
                afile=uigetfile('*.m');
                varargin={afile};
            case 4
                varargin = {'-s'};
            case 5
                varargin = {'-a'};
        end
    end
    if length(varargin) == 1 && strncmp(varargin{1}, '-a', 2)
        addformattershortcut;
        return;
    end
    default_format_settings_fact = struct('exception_anonymous_fun', true, 'remove_space_anonymous_fun', false, 'linebreak_after_semicolon', true, 'remove_double_space', true, 'remove_unneeded_spaces', false, ...%run only once with true, otherwise slow!
        'addspace_before_comment', true, 'addspace_before', {{'~'}}, 'addspace_around_no_unary', {{'+', '-'}}, 'addspace_after', {{',', ';'}}, 'addspace_around', {{'=', '/', '\', '*', '.*', './', '.\', '|', '||', '&', '&&', '>', '<', '>=', '<=', '==', '~='}}, 'remove_semicolons', true, ...% remove unneccesary semicolons after end, function,for,switch,case, if, while statements
        'remove_semicolons_keywords', {{'end', 'function', 'for', 'parfor', 'switch', 'case', 'if', 'while', 'catch', 'else', 'elseif'}}, 'replace_tabs_w_space', true, 'update_indent', true, 'indent_keywords', {{}}, ...%only mention keywords that should indended differently (see indend_keywords)
        'indent_line_extension', 4, 'FunctionIndentingFormat', {{'AllFunctionIndent'}}, ...ClassicFunctionIndent or MixedFunctionIndent or AllFunctionIndent
        'add_end_to_oldstylefunctions' , true, ...%works only if FunctionIndentingFormat= AllFunctionIndent
        'MaxWidth', 75, 'RemoveLineWraps', false, 'WrapLongLines', false, 'WrapLongComments', false );
    meta_settings = {'exception_anonymous_fun', 'No spacing of anonymous functions', 'logical'; 'remove_space_anonymous_fun', 'Remove unnecessary spaces in anonymous functions', 'logical'; 'linebreak_after_semicolon', 'Split lines with multiple statements', 'logical'; 'remove_double_space', 'Remove all double spaces (except indent)', 'logical'; 'remove_unneeded_spaces', 'Remove all unnecessary spaces (except indent)', 'logical'; 'addspace_before_comment', 'Allways one space before a single line comment', 'logical'; 'addspace_before', 'Add space before these opers', 'cell'; 'addspace_around_no_unary', 'Analyse if +/- is unary or not. Space before unary and space around binary + or -', 'cell'; 'addspace_after', 'Add space after these opers', 'cell'; 'addspace_around', 'Add space before and after these opers', 'cell'; 'remove_semicolons', 'Remove unneccesary semicolons (or commas) after indicated keywords', 'logical'; 'remove_semicolons_keywords', 'Keywords for removing unnecesary semicolons (or commas)', 'cell'; 'replace_tabs_w_space', 'Replace spaces with tab (recommended for correct indendation)', 'logical'; 'update_indent', 'Update indent', 'logical'; 'indent_keywords', 'Indendation keywords and rules', 'cell'; 'indent_line_extension', 'Number of extra spaces for line extensions', 'double'; 'FunctionIndentingFormat', 'Style for indenting functions', 'cell'; 'add_end_to_oldstylefunctions', 'Add "end" to old-style functions', 'logical'
        'MaxWidth', 'Maximum number of columns for wrapping', 'int32'; 'RemoveLineWraps', 'Merge lines that were wrapped with three dots [...]', 'logical'; 'WrapLongLines', 'Wrap long lines of code (see MaxWidth)', 'logical'; 'WrapLongComments', 'Wrap long comments (see MaxWidth)', 'logical'};
    
    try
        %use the current indendation settings from matlab, using the
        %settings API (after MATLAB 2018)
        sett = settings;
        default_format_settings_fact.MaxWidth = sett.matlab.editor.language.matlab.comments.MaxWidth.ActiveValue;
        default_format_settings_fact.FunctionIndentingFormat = {sett.matlab.editor.language.matlab.FunctionIndentingFormat.ActiveValue};
    catch
        %no error in older matlab versions
    end
    settings_file = [mfilename('fullpath') '.mat'];
    if exist(settings_file, 'file')
        load(settings_file, 'format_settings');
        default_format_settings = mergestruc(format_settings, default_format_settings_fact);
    else
        default_format_settings = default_format_settings_fact;
    end
    fromfile = '';
    tofile = '';
    format_settings = [];
    settingsentered = false;
    options = {};
    testlines = {};
    %parse arguments simply on basis of their class
    for i = 1:length(varargin)
        if ischar(varargin{i})
            if strncmp(varargin{i}, '-', 1)
                options = [options varargin(i)];
            elseif isempty(fromfile)
                fromfile = varargin{i};
            else
                tofile = varargin{i};
            end
        elseif isstruct(varargin{i}) || any(strncmpi(options, '-s', 2))
            settingsentered = true;
            format_settings = varargin{i};
        else
            testlines = varargin{i};
        end
    end
    
    if any(strncmpi(options, '-s', 2))
        if isempty(format_settings)
            if settingsentered
                format_settings = default_format_settings_fact;
            else
                format_settings = default_format_settings;
            end
        else
            format_settings = mergestruc(format_settings, default_format_settings);
        end
        if ~settingsentered && nargout == 0
            format_settings1 = format_matlab_dlg(format_settings, meta_settings);
            if ~isempty(format_settings1)
                format_settings = format_settings1;
                settingsentered = true;
            end
        end
        if settingsentered
            save(settings_file, 'format_settings');
        end
        res = format_settings;
        return;
    end
    
    if isempty(format_settings)
        format_settings = default_format_settings;
    end
    
    
    format_settings = mergestruc(format_settings, default_format_settings);
    
    if ischar(format_settings.FunctionIndentingFormat) %oldstyle
        format_settings.FunctionIndentingFormat = {format_settings.FunctionIndentingFormat};
    end
    for i = 1:size(meta_settings, 1)
        if ~isa(format_settings.(meta_settings{i, 1}), meta_settings{i, 3})
            error('format:settings:class', 'Wrong class for field "%s", %s should be %s', meta_settings{i, 1}, class(format_settings.(meta_settings{i, 1})), meta_settings{i, 3});
        end
    end
    
    if any(strcmp(format_settings.addspace_around, '-')) || any(strcmp(format_settings.addspace_around, '+'))
        error('format_matlab:settings', 'Not allowed to add always spaces around + or - as that may change the meaning');
    end
    
    if any(strncmpi(options, '-d', 2) | strncmpi(options, '-t', 2))
        %run directory
        dotest = any(strncmpi(options, '-t', 2));
        errorfiles = {};
        if isempty(fromfile)
            fromfile = '*.m';
        end
        f = find(fromfile == '*', 1);
        if ~isempty(f)
            thedir = fromfile(1:f - 1);
        end
        if exist(fromfile, 'file') == 2
            [path, name, ext] = fileparts(fromfile);
            d = struct('name', [name ext], 'folder', path);
        else
            d = dir(fromfile);
            if ~isfield(d, 'folder')
                for i = 1:length(d)
                    d(i).folder = thedir;
                end
            end
        end
        lastwarn('');
        if ~dotest && strcmp(questdlg(sprintf('Are you sure you want to format %d files in place (no undo possible!)', length(d)), 'Warning', 'Yes', 'No', 'No'), 'No')
            return;
        end
        for i = 1:length(d)
            fname = fullfile(d(i).folder, d(i).name);
            fprintf('Formatting %s\n', fname)
            if dotest
                mdir = fileparts(mfilename('fullpath'));
                tmpfile = fullfile(mdir, 'tmp_format_matlab_test.m');
                try
                    s = checkcode(fname, '-id');
                    format_matlab(fname, tmpfile, format_settings);
                    [lastmsg, lastid] = lastwarn;
                    if strcmp(lastid, 'format_matlab:nomatchingend')
                        error(lastid, lastmsg);
                    end
                    s2 = checkcode(tmpfile, '-id');
                    ndx = ~(strcmp({s2.id}, 'FNDEF') | strcmp({s2.id}, 'MCFIL')); %ignore errors due to wrong filename
                    s2 = s2(ndx);
                    if checkwarn(s, s2 , fname, tmpfile)
                        error('format_matlab:test', 'other warnings/errors in new file');
                    end
                catch err
                    if ~strcmp(err.identifier, 'format_matlab:test')
                        getReport(err)
                    end
                    errorfiles{end + 1} = fname;
                    lastwarn('');
                end
            else
                format_matlab(fname, fname, format_settings);
            end
        end
        if dotest
            res1 = cell(size(errorfiles));
            for i = 1:length(errorfiles)
                res1{i} = sprintf('errors in %s <a href="matlab:format_matlab(''-t'',''%s'')">run again</a> <a href="matlab:edit(''%s'')">edit</a>', errorfiles{i}, errorfiles{i}, errorfiles{i});
                disp(res1{i});
                %  checkcode(tmpfile, '-id');
            end
            if nargout > 0
                res = res1';
            end
        end
        return;
    end
    
    
    AllFunctionIndent = false;
    %nindent before and after the line with keyword [first digit is push (1) or
    %pop (-1) the pupuplist, second optional digit is the number of spaces to push)
    %do not change this list of keywords,you can adapt it in the
    %settings.indent_keywords just only give the words that need be changed
    indent_keywords = {'arguments', 0, [1 4]; ... %1
        'case', [1 -4], -1; ... %2
        'catch', [1 -4], -1; ... %3
        'classdef', 0, [1 4]; ... %4
        'else', [1 -4], -1; ... %5
        'elseif', [1 -4], -1; ... %6
        'end', -1, 0; ... %7
        'enumeration', 0, [1 4]; ... %8
        'for', 0, [1 4]; ... %9
        'function', 0, [1 4]; ... %10
        'if', 0, [1 4]; ... %11
        'methods', 0, [1 4]; ... %12
        'otherwise', [1 -4], -1; ... %13
        'parfor', 0, [1 4]; ... %14
        'properties', 0, [1 4]; ... %15
        'switch', 0, [1 8]; ... %16
        'try', 0, [1 4]; ... %17
        'while', 0, [1 4]; ... %18
        'spmd', 0, [1 4]; ...%19
        'global', 0, 0; ...%20
        'persistent', 0, 0; ... %21
        'break', 0, 0; ...%22
        'return', 0, 0 ...%23
        };
    none_keyword = 24;
    class_keywords = [8 12 15];
    classdef_keyword = 4;
    arguments_keyword = 1;
    function_keyword = 10;
    else_keyword = 5;
    if_keyword = 11;
    
    if format_settings.remove_unneeded_spaces
        %remove all unneccerary spaces (some need to stay)
        %leave one space after these keywords
        
        format_settings.reservedwords_with_equation = find(ismember(indent_keywords(:, 1), {'function', 'for', 'parfor', 'classdef', 'switch', 'case', 'catch', 'methods', 'if', 'elseif', 'properties', 'while'}));
        %do not change spacing after these keywords
        format_settings.reservedwords_with_commandstyle = find(ismember(indent_keywords(:, 1), {'global', 'end', 'break', 'return', 'persistent', 'otherwise', 'else', 'try', 'case', 'spmd'}));
    end
    if format_settings.update_indent
        %make sure the order of the keywords is correct
        for i = 1:size(format_settings.indent_keywords, 1)
            ndx = strcmp(indent_keywords(:, 1), format_settings.indent_keywords{i, 1});
            indent_keywords(ndx, :) = format_settings.indent_keywords(i, :);
        end
        if strcmp(format_settings.FunctionIndentingFormat, 'ClassicFunctionIndent')
            format_settings.indent_keywords{function_keyword, 3} = 0;
        elseif strcmp(format_settings.FunctionIndentingFormat, 'AllFunctionIndent')
            AllFunctionIndent = true;
        end
    end
    format_settings.indent_keywords = indent_keywords;
    if format_settings.remove_semicolons
        %replace keywords by index numbers
        if iscell(format_settings.remove_semicolons_keywords)
            format_settings.remove_semicolons_keywords = find(ismember(format_settings.indent_keywords(:, 1), format_settings.remove_semicolons_keywords));
        end
    end
    
    changed = false;
    lines = {};
    if strncmpi(fromfile, '-c', 2) || isempty(fromfile) && isempty(testlines)
        currentEditorPage = matlab.desktop.editor.getActive();
        try
            selection = currentEditorPage.Selection;
            lines = str2cell(currentEditorPage.Text);
        catch err
            if ~strcmp(err.identifier, 'MATLAB:needMoreRhsOutputs')
                rethrow(err);
            end
            disp('No file opened');
        end
    elseif ~isempty(testlines)
        lines = testlines;
    else
        fid = fopen(fromfile, 'r');
        if verLessThan('matlab', '8')
            lines = textscan(fid, '%s', 'delimiter', '\n', 'whitespace', '', 'bufsize', 1000000); %#ok<BUFSIZE>
        else
            lines = textscan(fid, '%s', 'delimiter', '\n', 'whitespace', '');
        end
        lines = lines{1};
        fclose(fid);
    end
    [fs, spfs] = doparseeq1(lines);
    if isempty(fs)
        if ~isempty(tofile) && ~strcmp(fromfile, tofile)
            copyfile(fromfile, tofile);
        end
        return;
    end
    if isempty(spfs.elems)
        return;
    end
    %merge anonymous functions before searching elements
    if format_settings.exception_anonymous_fun
        [spfs, fs, changed] = exception_anonymous_fun(format_settings, changed, spfs, fs);
    end
    [fs_prop, fspaces, fcomment, bracket_level, flbracket, flbrack] = get_fs_properties(format_settings, fs, spfs, none_keyword);
    if format_settings.replace_tabs_w_space
        [spfs, fs, changedtabs] = replace_tabs_w_space(false, fs, fs_prop, spfs);
        if changedtabs
            changed = true;
            [fs_prop, fspaces, fcomment, bracket_level, flbracket, flbrack] = get_fs_properties(format_settings, fs, spfs, none_keyword);
        end
    end
    if format_settings.remove_unneeded_spaces
        [spfs, fs, changed] = remove_unneeded_spaces(format_settings, changed, fspaces, bracket_level, spfs, fs, fs_prop);
        if changed
            [fs_prop, fspaces, fcomment, bracket_level, flbracket, flbrack] = get_fs_properties(format_settings, fs, spfs, none_keyword);
        end
    end
    if format_settings.linebreak_after_semicolon
        [fs, changedlines] = linebreak_after_semicolon(false, fspaces, fs, fs_prop);
        if changedlines
            changed = true;
            %reanalyse
            newlines = cell(size(fs));
            for i = 1:length(fs)
                newlines{i} = sprintf('%s', fs{i}{:});
            end
            newlines = str2cell(sprintf('%s\n', newlines{:}));
            [fs, spfs] = doparseeq1(newlines);
            %spfs = sparse_cell(fs); %sparse cell for fast searching
            %merge anonymous functions before searching elements
            if format_settings.exception_anonymous_fun
                [spfs, fs, changed] = exception_anonymous_fun(format_settings, changed, spfs, fs);
            end
            [fs_prop, fspaces, fcomment, bracket_level, flbracket, flbrack] = get_fs_properties(format_settings, fs, spfs, none_keyword);
        end
    end
    flinebreak = spfs.strcmp(sprintf('\n')); %#ok<SPRINTFN>
    
    if format_settings.remove_semicolons
        [fs, changed] = remove_semicolons(format_settings, fspaces, flinebreak, changed, fs, fs_prop, spfs);
    end
    if format_settings.RemoveLineWraps
        [spfs, fs, ischanged] = RemoveLineWraps(changed, fs, fs_prop, spfs);
        if ischanged
            changed = true;
            flinebreak = spfs.strcmp(sprintf('\n')); %#ok<SPRINTFN>
            [fs_prop, fspaces, fcomment, bracket_level, flbracket, flbrack] = get_fs_properties(format_settings, fs, spfs, none_keyword);
        end
    end
    if format_settings.remove_double_space
        [fs, changed] = remove_double_space(changed, fspaces, flinebreak, fs, fs_prop);
    end
    if format_settings.addspace_before_comment
        faddspace_before_comment = fcomment;
    else
        faddspace_before_comment = sparse(size(fcomment, 1), size(fcomment, 2));
    end
    [funary_minus, fminus] = find_unary_minus(format_settings, fspaces, bracket_level, fs_prop, spfs);
    faddspace_before = spfs.ismember(format_settings.addspace_before) | funary_minus;
    faddspace_after = spfs.ismember(format_settings.addspace_after);
    flallbracket = flbracket | flbrack;
    hasbracketbefore = [zeros(size(flallbracket, 1), 1) flallbracket(:, 1:end - 1)];
    faddspace_before = faddspace_before & ~hasbracketbefore;
    faddspace_around = spfs.ismember(format_settings.addspace_around) | fminus;
    spaceafter = faddspace_after | faddspace_around;
    spacebefore = faddspace_before_comment | faddspace_before | faddspace_around;
    hasspacebefore = [zeros(size(fspaces, 1), 1) fspaces(:, 1:end - 1)] | [zeros(size(flinebreak, 1), 1) flinebreak(:, 1:end - 1)];
    hasspaceafter = [fspaces(:, 2:end) zeros(size(fspaces, 1), 1)];
    spaceafter(fspaces | hasspaceafter) = false;
    spacebefore(fspaces | hasspacebefore) = false;
    %merge space before and spaceafer (to avoid double spaces)
    spaceafter = spaceafter | [spacebefore(:, 2:end) zeros(size(fspaces, 1), 1) ];
    ndx = fs_prop.fndx;
    for i = 1:length(ndx)
        fs1 = fs{ndx(i)};
        fspace = find(spaceafter(ndx(i), 1:fs_prop.n(ndx(i)) - 1));
        if any(fspace)
            changed = true;
            for j = 1:length(fspace)
                fs1{fspace(j)} = [fs1{fspace(j)} ' '];
            end
        end
        fs{ndx(i)} = fs1;
    end
    
    
    
    
    if format_settings.update_indent
        %first check if old style functions are used
        indentlevel = full(sum(cumprod(double(fspaces), 2), 2));
        afterindentndx = 1;
        newindentndx = 1;
        indent_stack = zeros(1, 30);
        keyword_stack = zeros(1, 30);
        nfunctions = 0;
        indents = zeros(size(fs));
        isfunction = false(size(fs));
        for i = 1:length(fs)
            keywords = [fs_prop.firstkeyword{i} fs_prop.xtra{i}];
            for j = 1:length(keywords)
                %remove class keywords if they are not in the context of
                %class definition
                if any(keywords(j) == arguments_keyword)
                    if i == 1 || ~(fs_prop.firstkeyword{i - 1} == function_keyword)
                        keywords(j) = none_keyword;
                    end
                end
                if any(keywords(j) == class_keywords)
                    if ~(newindentndx == 2 && any(keyword_stack(1:newindentndx) == classdef_keyword))
                        keywords(j) = none_keyword;
                    end
                end
            end
            for k = 1:length(keywords)
                if keywords(k) <= size(format_settings.indent_keywords, 1)
                    if keywords(k) == 10 %function
                        nfunctions = nfunctions + 1;
                        isfunction(i) = true;
                    end
                    [newindentndx, indent_stack, keyword_stack] = popindent(newindentndx, indent_stack, format_settings.indent_keywords{keywords(k), 2}, keyword_stack, keywords(k));
                    [afterindentndx, indent_stack, keyword_stack] = popindent(newindentndx, indent_stack, format_settings.indent_keywords{keywords(k), 3}, keyword_stack, keywords(k));
                end
                indents(i) = indent_stack(newindentndx); %debug
                newindentndx = afterindentndx;
            end
        end
        oldstylefunctions = nfunctions > 0 && nfunctions * 4 == indent_stack(newindentndx);
        if ~oldstylefunctions && indent_stack(newindentndx) > 0 && nfunctions > 2
            fndx = find(isfunction);
            disp('Possible problem before this function:');
            diffun = diff(indents(isfunction));
            aa = fs(fndx([diffun ~= round(mean(diffun)) ; false]));
            for l = 1:length(aa)
                fprintf('%s', aa{l}{:});
                disp(' ');
            end
        end
        nfunctions = 0;
        newindentndx = 1;
        last_nonempty = 1;
        indent_stack = zeros(1, 30);
        keyword_stack = zeros(1, 30);
        fspace = false;
        for i = 1:length(fs)
            fs1 = fs{i};
            keywords = [fs_prop.firstkeyword{i} fs_prop.xtra{i}];
            n = fs_prop.n(i);
            if ~all(fspace) && ~isempty(fspace)
                last_nonempty = max(1, i - 1);
            end
            for j = 1:length(keywords)
                %remove class keywords if they are not in the context of
                %class definition
                if any(keywords(j) == arguments_keyword)
                    if i == 1 || ~(fs_prop.firstkeyword{i - 1} == function_keyword)
                        keywords(j) = none_keyword;
                    end
                end
                if any(keywords(j) == class_keywords)
                    if ~(newindentndx == 2 && any(keyword_stack(1:newindentndx) == classdef_keyword))
                        keywords(j) = none_keyword;
                    end
                end
            end
            fspace = full(fspaces(i, 1:n));
            afterindentndx = newindentndx;
            for j = 1:length(keywords)
                if keywords(j) <= size(format_settings.indent_keywords, 1)
                    if keywords(j) == 10 && oldstylefunctions %function
                        if AllFunctionIndent
                            [newindentndx, indent_stack, keyword_stack] = popindent(afterindentndx, indent_stack, [-1 0], keyword_stack, keywords(j));
                            [afterindentndx, indent_stack, keyword_stack] = popindent(newindentndx, indent_stack, format_settings.indent_keywords{keywords(j), 3}, keyword_stack, keywords(j));
                            if nfunctions > 0 && format_settings.add_end_to_oldstylefunctions
                                fs{last_nonempty}{end} = sprintf('%s\n%send', fs{last_nonempty}{end}, repmat(' ', 1, indent_stack(newindentndx)));
                            end
                            nfunctions = nfunctions + 1;
                        end
                    else
                        [newindentndx, indent_stack, keyword_stack] = popindent(afterindentndx, indent_stack, format_settings.indent_keywords{keywords(j), 2}, keyword_stack, keywords(j));
                        [afterindentndx, indent_stack, keyword_stack] = popindent(newindentndx, indent_stack, format_settings.indent_keywords{keywords(j), 3}, keyword_stack, keywords(j));
                    end
                end
            end
            if fs_prop.formatting(i)
                %not very elegant but this repairs a wrong indent if
                %you use "else if" instead of "elseif" recommended
                if length(fs_prop.firstkeyword{i}) == 1 && (fs_prop.firstkeyword{i}(1) == else_keyword) && (length(fs_prop.xtra{i}) == 1 && fs_prop.xtra{i}(1) == if_keyword)
                    h = -4;
                else
                    h = 0;
                end
                [fs1, changed] = checkindent(format_settings, changed, indent_stack(newindentndx) + h, indent_stack(afterindentndx), flinebreak(i, 1:n), fspace, indentlevel(i), fs1);
                fs{i} = fs1;
            end
            newindentndx = afterindentndx;
            %is end
        end
        if oldstylefunctions && AllFunctionIndent && nfunctions > 0 && format_settings.add_end_to_oldstylefunctions
            if ~all(fspace) && ~isempty(fspace)
                last_nonempty = length(fs);
            end
            [newindentndx, indent_stack, ~] = popindent(afterindentndx, indent_stack, [-1 0], keyword_stack, 7);
            fs{last_nonempty}{end} = sprintf('%s\nend', fs{last_nonempty}{end});
        end
        if indent_stack(newindentndx) ~= 0
            warning('format_matlab:nomatchingend', 'No matching end found. Possibly a bug in the code')
        end
    end
    
    
    newlines = cell(size(fs));
    for i = 1:length(fs)
        newlines{i} = sprintf('%s', fs{i}{:});
    end
    if nargout > 0
        res = sprintf('%s\n', newlines{:});
        if ~isempty(testlines)
            res = str2cell(res);
        end
    end
    if changed || (~isempty(tofile) && ~strcmp(fromfile, tofile))
        if ~isempty(testlines)
            if nargout == 0
                fprintf('%s\n', newlines{:});
            end
        elseif isempty(fromfile)
            currentEditorPage.Text = sprintf('%s\n', newlines{:});
            currentEditorPage.Selection = selection;
            [ ~, name, ext] = fileparts(currentEditorPage.Filename);
            fprintf('format_matlab: formatted "%s%s"\n', name, ext);
        elseif ~isempty(tofile)
            fid = fopen(tofile, 'w');
            fprintf(fid, '%s\n', newlines{:});
            fclose(fid);
        else
            fprintf('%s\n', newlines{:});
        end
    else
        if isempty(fromfile) && isempty(testlines)
            currentEditorPage.Selection = selection;
        end
        disp('format_matlab: no changes in spacing')
    end
end
function isproblem = checkwarn(s1, s2 , ~, tmpfile)
    ndx = find(strcmp({s2.id}, 'MSNU'));
    sel = true(size(s2));
    if ~isempty(ndx)
        for i = length(ndx): -1:1
            warn = s2(ndx(i));
            fid = fopen(tmpfile, 'r');
            for j = 1:warn.line
                line = fgetl(fid);
            end
            fclose(fid);
            suppres_msg = regexp(line(warn.column(1):warn.column(2)), '(?<=[<])[^>]*', 'match', 'once');
            if ~isempty(suppres_msg)
                ndx2 = [s2.line] < warn.line;
                ndx1 = find(ndx2 & strcmp({s2.id}, suppres_msg));
                if ~isempty(ndx1)
                    lines = abs([s2(ndx1).line] - warn.line);
                    %remove the one with the nearest distance
                    ndx1 = ndx1(lines == min(lines));
                    sel(ndx1) = false;
                    sel(ndx(i)) = false;
                end
            end
        end
    end
    s2 = s2(sel);
    [ids, ndx] = unique([{s1.id} {s2.id}]);
    isproblem = false;
    if any(strcmp(ids, 'BDFIL')) %bad file name no warnings are generated
        return;
    end
    msgs = [{s1.message} {s2.message}];
    msgs = msgs(ndx);
    for i = 1:length(ids)
        nbefore = sum(double(strcmp({s1.id}, ids{i})));
        nafter = sum(double(strcmp({s2.id}, ids{i})));
        if nafter > nbefore
            if ~strcmp(ids{i}, 'MSNU')
                isproblem = true;
            end
            f = find(strcmp({s2.id}, ids{i}));
            lines = '';
            for j = 1:length(f)
                lines = sprintf('<a href="matlab:opentoline(''%s'',%d,%d)">L %d</a>', tmpfile, s2(f(j)).line, s2(f(j)).column(1), s2(f(j)).line);
            end
            h = 2;
        else
            h = 1;
            lines = '';
        end
        if nafter ~= nbefore
            fprintf(h, '(%d/%d) %s %s%s\n', nbefore, nafter, ids{i}, msgs{i}, lines);
        end
    end
end
function [fs_prop, fspaces, fcomment, bracket_level, flbracket, flbrack] = get_fs_properties(format_settings, fs, spfs, none_keyword)
    fsemicolon = spfs.strcmp(';');
    fspaces = spfs.strcmp(' ');
    fformat_off = any(spfs.strcmpi('%#format<off>'), 2);
    fformat_on = any(spfs.strcmpi('%#format<on>'), 2);
    fcomment = spfs.strncmp('%', 1);
    fs_prop.first_non_space = num2cell(sum(cumsum(double(~(fspaces | fcomment)), 2) == 0, 2) + 1);
    flbracket = spfs.ismember({'[', '{'});
    frbracket = spfs.ismember({']', '}'});
    bracket_level = cumsum(double(flbracket - frbracket), 2);
    fcomma = spfs.strcmp(',');
    flbrack = spfs.strcmp('(');
    fkeywords = spfs.ismemberndx(format_settings.indent_keywords(:, 1));
    anybrack_level = bracket_level + cumsum(double(flbrack - spfs.strcmp(')')), 2);
    endoflines = (fsemicolon | fcomma) & (anybrack_level == 0);
    fend = spfs.ismember({'end', 'if'}) & anybrack_level == 0;
    fs_prop.specialblock = false(size(fs));
    fs_prop.formatting = false(size(fs));
    fs_prop.command_style = false(size(fs));
    fs_prop.eolines = cell(size(fs));
    fs_prop.firstkeyword = cell(size(fs));
    fs_prop.xtra = cell(size(fs));
    fs_prop.lastsemicolon = zeros(size(fs));
    fs_prop.n = zeros(size(fs));
    specialblock = false;
    formatting = true;
    for i = 1:length(fs)
        fs1 = fs{i};
        n = length(fs1);
        fs_prop.n(i) = n;
        xtra = fend(i, 1:n);
        fspace = full(fspaces(i, 1:n));
        if ~isempty(fs1) && fs_prop.first_non_space{i}(1) <= n
            xtra(fs_prop.first_non_space{i}(1)) = false;
            fs_prop.firstkeyword{i} = fkeywords(i, fs_prop.first_non_space{i}(1));
            if fs_prop.firstkeyword{i} == 0
                fs_prop.firstkeyword{i} = none_keyword;
            end
        else
            fs_prop.firstkeyword{i} = none_keyword;
        end
        eolines = find(endoflines(i, 1:n)); %more commands on one line
        fs_prop.lastsemicolon(i) = 0;
        if ~isempty(eolines)
            %remove the trailing semicolon (or comma)
            k = eolines(end) + 1;
            while k <= n && (fspace(k) || fcomment(i, k))
                k = k + 1;
            end
            if k > n
                fs_prop.lastsemicolon(i) = eolines(end);
                eolines(end) = [];
            end
        end
        
        if fformat_off(i)
            formatting = false;
        end
        if fformat_on(i)
            formatting = true;
        end
        fs_prop.formatting(i) = formatting;
        for j = 1:length(eolines)
            k = eolines(j) + 1;
            while k < n && fspace(k)
                k = k + 1;
            end
            if k <= n && ~fspace(k)
                fs_prop.first_non_space{i} = [fs_prop.first_non_space{i} k];
                xtra(k) = false;
                kw = fkeywords(i, k);
                if kw == 0
                    kw = none_keyword;
                end
                fs_prop.firstkeyword{i} = [fs_prop.firstkeyword{i} kw];
            end
        end
        fs_prop.eolines{i} = eolines;
        for k = 1:length(fs_prop.firstkeyword{i})
            %check for assignemnts to methods enumeration etc
            if any(fs_prop.firstkeyword{i}(k) == [1 8 12 15])
                k1 = fs_prop.first_non_space{i}(k) + 1;
                while k1 < fs_prop.n(i) && fspace(k1)
                    k1 = k1 + 1;
                end
                if k1 < fs_prop.n(i) && any(strncmp(fs1{k1}, {'=', '.'}, 1))
                    fs_prop.firstkeyword{i}(k) = none_keyword;
                end
            end
            if any(fs_prop.firstkeyword{i}(k) == [1 8]) % after keywords arguments,enumeration no formatting
                specialblock = true;
            end
            if any(fs_prop.firstkeyword{i}(k) == 7) || fformat_on(i)
                specialblock = false;
            end
        end
        xtra = find(xtra);
        for h = 1:length(xtra)
            xtra(h) = fkeywords(i, xtra(h));
        end
        fs_prop.xtra{i} = xtra;
        fs_prop.specialblock(i) = specialblock;
        fs_prop.command_style(i) = is_command_style(fs1, fspace);
        if fs_prop.command_style(i)
            fs_prop.firstkeyword{i} = none_keyword;
            fs_prop.first_non_space{i} = fs_prop.first_non_space{i}(1);
            fs_prop.xtra{i} = [];
        end
        %fs_prop.command_style= (fs_prop.firstkeyword{i}(1) == 21 && is_command_style(fs1, fspace));
        %do not change text after command style (but remove double
        %spaces)
    end
    fs_prop.fndx = find(fs_prop.formatting & ~fs_prop.specialblock & ~fs_prop.command_style);
end

function format_settings1 = mergestruc(format_settings, default_format_settings)
    %merge format_settings with default_format_settings
    if isempty(format_settings)
        format_settings1 = default_format_settings;
        return;
    end
    format_settings1 = default_format_settings; %keep the same order of fields
    fnames1 = fieldnames(default_format_settings);
    fnames2 = fieldnames(format_settings);
    for k = 1:length(fnames2)
        if any(strcmp(fnames2{k}, fnames1))
            format_settings1.(fnames2{k}) = format_settings.(fnames2{k});
        else
            error('format:unknown:field', 'Unknown field "%s" in settings', fnames2{k})
        end
    end
end


function [spfs, fs, changed] = replace_tabs_w_space(changed, fs, fs_prop, spfs)
    ftabs = spfs.strcmp(char(9));
    ischanged = false;
    ndx = fs_prop.fndx;
    for i = 1:length(ndx)
        ftab = ftabs(ndx(i), 1:fs_prop.n(ndx(i)));
        if any(ftab)
            fs1 = fs{ndx(i)};
            f = find(ftab);
            for k = length(f): -1:1
                fs1 = [fs1(1:f(k) - 1), {' ', ' ', ' ', ' '}, fs1(f(k) + 1:end)];
            end
            ischanged = true;
            fs{ndx(i)} = fs1;
            spfs(ndx(i), 1:length(fs1)) = fs1;
        end
    end
    if ischanged
        spfs.cleanup;
        changed = true;
    end
end
function [spfs, fs, changed] = RemoveLineWraps(changed, fs, fs_prop, spfs)
    fdotdotdots = spfs.strncmp('...', 3);
    ndx = fs_prop.fndx;
    for i = 1:length(ndx)
        fddd = find(fdotdotdots(ndx(i), 1:fs_prop.n(ndx(i))));
        if ~isempty(fddd)
            fs1 = fs{ndx(i)};
            for k = 1:length(fddd)
                if strcmp(strtrim(fs1{fddd(k)}), '...') %ignore trailing spaces
                    %note that if there is a comment after the dots the linebreak is not removed
                    fs1{fddd(k)} = '';
                    if fddd(k) < length(fs1) && strcmp(fs1{fddd(k) + 1}, sprintf('\n')) %#ok<SPRINTFN>
                        fs1{fddd(k) + 1} = '';
                    end
                end
            end
            changed = true;
            fs{ndx(i)} = fs1;
            spfs(ndx(i), 1:length(fs1)) = fs1;
        end
    end
end

function [funary_minus, fminus] = find_unary_minus(format_settings, fspaces, bracket_level, fs_prop, spfs)
    fminus = spfs.ismember(format_settings.addspace_around_no_unary);
    funary_minus = zeros(size(fminus));
    identifchar = ['0':'9', 'a':'z', 'A':'Z', '"', ')', ']'];
    [plusminus_i, plusminus_j] = find(fminus);
    for k = 1:length(plusminus_i)
        i = plusminus_i(k);
        j1 = plusminus_j(k);
        if ~fs_prop.command_style(i) && fs_prop.formatting(i) && ~fs_prop.specialblock(i)
            j = j1 - 1;
            while j >= 1 && fspaces(i, j)
                j = j - 1;
            end
            if j >= 1 && ~fspaces(i, j)
                elembefore = spfs{i, j};
                if any(strcmp(elembefore, {'+', '-'}))
                    fminus(i, j1) = false;
                else
                    if ~isempty(elembefore)
                        isidentifier = (any(elembefore(1) == identifchar) || any(strcmp(elembefore, {'''', '.'''}))) && ~any(strcmp(elembefore, {'function', 'for', 'parfor', 'classdef', 'switch', 'case', 'catch', 'methods', 'if', 'elseif', 'properties', 'while'}));
                        if ~isidentifier && elembefore(1) == '.' && length(elembefore) > 1
                            isidentifier = any(elembefore(2) == identifchar);
                        end
                    else
                        isidentifier = false;
                    end
                    if ~isidentifier
                        fminus(i, j1) = false;
                        funary_minus(i, j1) = true;
                    elseif bracket_level(i, j1) > 0
                        spacebefore = fspaces(i, j1 - 1);
                        spaceafter = j1 < size(fminus, 2) && fspaces(i, j1 + 1);
                        if spacebefore && ~spaceafter || any(strcmp(spfs{i, j1 - 1}, {'+', '-'})) || any(strcmp(spfs{i, j1 + 1}, {'+', '-'}))
                            fminus(i, j1) = false;
                            funary_minus(i, j1) = false;
                        end
                    end
                end
            else
                fminus(i, j1) = false;
                funary_minus(i, j1) = true;
            end
        else
            fminus(i, j1) = false;
        end
    end
    funary_minus = sparse(funary_minus);
end

function [spfs, fs, changed] = remove_unneeded_spaces(format_settings, changed, fspaces, popbracket, spfs, fs, fs_prop)
    ndx = fs_prop.fndx;
    ischanged = false;
    fidenfiers = spfs.any_regexp_match('([.][.])|(^[@][a-zA-Z0-9_]+$)|(^[\.a-zA-Z0-9\''"][.a-zA-Z0-9_''\-\+"]*$)');
    flbrack = spfs.ismember({')', ']', '}'});
    frbrack = spfs.ismember({'(', '[', '{'});
    for i = 1:length(ndx)
        fs1 = fs{ndx(i)};
        %do not remove within brackets (as spaces can be necessary)
        poplevels = full(popbracket(ndx(i), 1:fs_prop.n(ndx(i))));
        fspace = fspaces(ndx(i), 1:fs_prop.n(ndx(i)));
        fspace(poplevels > 0) = false;
        fidenf = full(double(frbrack(ndx(i), 1:fs_prop.n(ndx(i))))) * 4 + full(double(flbrack(ndx(i), 1:fs_prop.n(ndx(i))))) * 3 + full(double(fidenfiers(ndx(i), 1:fs_prop.n(ndx(i))))) * 2 + fspace;
        f = [strfind(fidenf, [2 1]) strfind(fidenf , [3 1])];
        for k = 1:length(f)
            k1 = f(k) + 1;
            while k1 < fs_prop.n(ndx(i)) && fidenf(k1) == 1
                k1 = k1 + 1;
            end
            if any(fidenf(k1) == [2 4])
                fspace(f(k) + 1) = false;
            end
        end
        %do not remove indentation spaces
        lines = [0 fs_prop.eolines{ndx(i)} length(fs1)];
        for j = 2:length(lines)
            fspace(lines(j - 1) + 1:fs_prop.first_non_space{ndx(i)}(j - 1) - 1) = false; %indent spaces
            % check for commandsty;e
            if is_command_style(fs1(lines(j - 1) + 1:lines(j)), fspaces(ndx(i), lines(j - 1) + 1:lines(j)))
                fspace(lines(j - 1) + 1:lines(j)) = false;
            end
            if any(fs_prop.firstkeyword{ndx(i)}(j - 1) == format_settings.reservedwords_with_equation)
                fspace(fs_prop.first_non_space{ndx(i)}(j - 1) + 1) = false;
            end
            if any(fs_prop.firstkeyword{ndx(i)}(j - 1) == format_settings.reservedwords_with_commandstyle)
                fspace(lines(j - 1) + 1:lines(j)) = false;
            end
        end
        %leave space before ...
        fbeforedots = find(strncmp('...', fs1, 3));
        if ~isempty(fbeforedots)
            for g = 1:length(fbeforedots)
                fs1{fbeforedots(g)} = [' ' fs1{fbeforedots(g)}];
            end
        end
        
        if any(fspace)
            ischanged = true;
            oldn = length(fs1);
            fs{ndx(i)} = fs1(~fspace);
            fs1 = [fs{ndx(i)} repmat({''}, 1, oldn - length(fs{ndx(i)}))];
            spfs(ndx(i), 1:length(fs1)) = fs1;
        end
    end
    if ischanged
        spfs = spfs.cleanup;
        changed = true;
    end
end

function [stackndx, indent_stack, keyword_stack] = popindent(stackndx, indent_stack, command, keyword_stack, keyword)
    if command(1) == -1 && stackndx > 1
        stackndx = stackndx - 1;
    elseif command(1) == 1
        stackndx = stackndx + 1;
        keyword_stack(stackndx) = keyword;
        indent_stack(stackndx) = indent_stack(stackndx - 1) + command(2);
    end
end

function [fs, changed] = remove_semicolons(format_settings, fspaces, flinebreak, changed, fs, fs_prop, spfs)
    else_keyword = 5;
    catch_keyword = 3;
    fidenfiers = spfs.any_regexp_match('(^[@][a-zA-Z0-9_]+$)|(^[/.a-zA-Z0-9\''"][.a-zA-Z0-9_''\-\+"]*$)');
    flbrack = spfs.strcmp(')');
    ndx = fs_prop.fndx;
    for i = 1:length(ndx)
        if any(fs_prop.firstkeyword{ndx(i)}(1) == format_settings.remove_semicolons_keywords) && fs_prop.lastsemicolon(ndx(i)) ~= 0
            hasmorelines = false;
            fidenf = full(double(flbrack(ndx(i), 1:fs_prop.n(ndx(i))))) * 3 + full(double(fidenfiers(ndx(i), 1:fs_prop.n(ndx(i))))) * 2 + fspaces(ndx(i), 1:fs_prop.n(ndx(i)));
            f = [strfind(fidenf, [2 1]) strfind(fidenf , [3 1])];
            if any(fs_prop.firstkeyword{ndx(i)}(1) == [else_keyword catch_keyword])
                hasmorelines = any(fidenf(fs_prop.first_non_space{ndx(i)}(1) + 1:end) == 2);
            else
                for k = 2:length(f)
                    k1 = f(k) + 1;
                    while k1 < fs_prop.n(ndx(i)) && fidenf(k1) == 1
                        k1 = k1 + 1;
                    end
                    if k1 < fs_prop.n(ndx(i)) && fidenf(k1) == 2
                        hasmorelines = true;
                    end
                end
            end
            
            if ~hasmorelines && isempty(fs_prop.eolines{ndx(i)}) && ~any(flinebreak(ndx(i), 1:fs_prop.n(ndx(i))))
                changed = true;
                fs1 = fs{ndx(i)};
                if fs_prop.lastsemicolon(ndx(i)) > 0 && any(strcmp(fs1{fs_prop.lastsemicolon(ndx(i))}, {';', ','})) %check for semicolon may be unnecessary
                    fs1(fs_prop.lastsemicolon(ndx(i))) = {''};
                end
                fs{ndx(i)} = fs1;
            end
        end
    end
end

function iscomm = is_command_style(fs, fspace)
    %most complex: determine if the line is command style
    %
    % these opers kill the command_style, only if they are surrounded by
    % space
    opers_with_space = {'.^', '^', '.*', './', '.\', '*', '/', '\', '+', '-', ':', '<', '<=', '>', '>=', '==', '~=', '&', '|', '&&', '||'};
    %these opers always kill the command style
    opers_without_space = {'(', '='};
    %after reserved words, spaces can be added
    reservedwords = {'function', 'for', 'parfor', 'global', 'end', 'break', 'return', 'persistent', 'otherwise', 'else', 'classdef', 'switch', 'case', 'try', 'catch', 'methods', 'properties' 'if', 'elseif', 'case', 'while'};
    iscomm = false;
    if length(fs) > 1
        f1 = find(~fspace, 1);
        if isempty(f1)
            f1 = 1;
        end
        if f1 < length(fs)
            h = 1;
            while f1 + h < length(fspace) && fspace(f1 + h)
                h = h + 1;
            end
            %at least one space should be after a function name (a fucntion
            %is a identifier that cannot start with _ or number)
            if h > 1 && ~(any(strcmp(fs{f1 + h}, opers_without_space)) || ((f1 + h + 1 < length(fs)) && fspace(f1 + h + 1) && any(strcmp(fs{f1 + h}, opers_with_space))))
                iscomm = ~any(strcmp(fs{f1}, reservedwords)) && ~isempty(regexp(fs{f1}, '^[A-Za-z][A-Za-z_0-9]*$', 'once'));
            end
        end
    end
end

function [fs1, changed] = checkindent(settings, changed, newindent, afterindent, flinebreak, fspace, indentlevel, fs1)
    if settings.update_indent
        newindent = max(0, newindent);
        if newindent ~= indentlevel
            changed = true;
            fs1 = changeindent(fs1, 1, newindent - indentlevel);
        end
        if any(flinebreak) %&& settings.indent_line_extension > 0
            fbreak = find(flinebreak);
            breakindent = afterindent + settings.indent_line_extension;
            for i = 1:length(fbreak)
                if fbreak(i) < length(fs1)
                    oldindent = sum(cumprod(double(fspace(fbreak(i) + 1:end))));
                    if oldindent ~= breakindent
                        changed = true;
                        fs1 = changeindent(fs1, fbreak(i) + 1, breakindent - oldindent);
                    end
                end
            end
        end
    end
end

function fs = changeindent(fs, starti, nspaces)
    if ~isempty(fs)
        if nspaces < 0
            %note nspaces<0
            fs(starti:starti - nspaces - 1) = {''};
        else
            fs{starti} = [repmat(' ', 1, nspaces) fs{starti}];
        end
    end
end

function [fs, changed] = linebreak_after_semicolon(changed, fspaces, fs, fs_prop)
    ndx = find(~fs_prop.specialblock & fs_prop.formatting);
    for i = 1:length(ndx)
        if ~isempty(fs_prop.eolines{ndx(i)})
            fs1 = fs{ndx(i)};
            eolines = fs_prop.eolines{ndx(i)};
            f1 = find(~fspaces(ndx(i), 1:fs_prop.n(ndx(i))), 1);
            if isempty(f1)
                f1 = 1;
            end
            newline = sprintf('\n%s', repmat(' ', f1 - 1, 1));
            for j = 1:length(eolines)
                if strcmp(fs1{eolines(j)}, ',')
                    fs1{eolines(j)} = newline;
                elseif eolines(j) < length(fs1) && strcmp(fs1{eolines(j) + 1}, ';') && strcmp(fs1{eolines(j)}, ';')
                    %remove double semicolons
                    fs1{eolines(j)} = '';
                else
                    fs1{eolines(j)} = [fs1{eolines(j)} newline];
                end
                changed = true;
            end
            fs{ndx(i)} = fs1;
        end
        
    end
end

function [fs, changed] = remove_double_space(changed, fspaces, flinebreak, fs, fs_prop)
    ndx = find(~fs_prop.specialblock & fs_prop.formatting); %change this
    for j = 1:length(ndx)
        fs1 = fs{ndx(j)};
        if ~isempty(fs1)
            fspace = full(fspaces(ndx(j), 1:fs_prop.n(ndx(j))));
            k = length(fspace);
            %trim trailing spaces
            while k > 1 && fspace(k)
                fs1(k) = {''};
                changed = true;
                fspace(k) = false;
                k = k - 1;
            end
            fbreak = find(flinebreak(ndx(j), 1:fs_prop.n(ndx(j))));
            if isempty(fbreak)
                %for speed
                f1 = find(~fspace, 1);
                if any(f1)
                    ndx1 = strfind(fspace(f1:length(fs1)), [1 1]) + f1 - 1;
                    if any(ndx1)
                        fs1(ndx1) = {''};
                        changed = true;
                    end
                end
            else
                fline = [0 fbreak length(fs1)];
                for i = 2:length(fline)
                    f1 = find(~fspace(fline(i - 1) + 1:fline(i)), 1);
                    if ~isempty(f1)
                        ndx1 = strfind(fspace(fline(i - 1) + f1:fline(i)), [1 1]) + f1 + fline(i - 1) - 1;
                        if any(ndx1)
                            fs1(ndx1) = {''};
                            changed = true;
                        end
                    end
                end
            end
            
            fs{ndx(j)} = fs1;
        end
    end
end



function [fss, spfs] = doparseeq1(mfile)
    %this pattern for regexp parses the whole m file into
    %comments,opers,identifiers,strings,numbers etc.
    pattern = ['(?<comment>[%].*)|', ... %comment
        '(?<string>(?<![a-zA-Z0-9_\)\]\}\''])[\''](([^''])|(['']{2}))*[\''])|', ... %single quote string, note: cannot be preceeded by variable or closing brackets
        '(?<string2>["][^"]*["])|', ... %double quote string
        '(?<oper>[.][\''])|([\''])|', ... % other single quotes are ctranspose or transpose
        '(?<dots>[.][.][.].*)|' ... %dots and what follows
        '(?<oper>[.$][\*/\\^''])|' ... %dot opers
        '(?<number>[.]?[0-9]+[.]?[0-9]*([eEdD][-+]?)?[0-9]*)|' ... %numbers
        '(?<funhandle>[\@][A-Za-z]?[A-Za-z0-9_]*)|', ... %function handles only @ for anonymus
        '(?<field>[.][A-Za-z_]+[A-Za-z0-9_]*)|', ... %fields
        '(?<symbol>[A-Za-z_][A-Za-z0-9_#]*)|' ... %symbols
        '(?<oper>[&|=~<>][&|=<>])|' ... %longer opers
        '(?<oper_or_char>.)']; %any other oper, brackets or char (never throw some parts away
    %    '([ .~><\(\)^\*\\/\+='':;,&|%!{}\[\]#@"])|[-\n]']; %any other char
    %it seems not possible to let 2 .*pi translate to "2" ".*" "pi"
    %add space between 2 and .*pi
    if size(mfile, 2) > 1
        mfile = mfile';
    end
    lines = regexprep(mfile, '([0-9])([\.][\*\/])', '$1 $2');
    %themodel1=eqs;
    
    %******magical function******!
    fss = regexp(lines, pattern, 'match');
    spfs = sparse_cell(fss);
    comm_on = any( spfs.any_regexp_match('[\%][\{][ ]*\t*$'), 2); %support new multiline comments
    comm_off = any(spfs.any_regexp_match('[\%][\}][ ]*\t*$'), 2);
    fiscomment = false(size(comm_on));
    istrue = false;
    for i = 1:length(comm_on)
        %         if istrue
        %             fss{i}=sprintf('%s',fss{i}{:});
        %         end
        if comm_on(i)
            istrue = true;
        elseif i > 1 && comm_off(i - 1)
            istrue = false;
        end
        fiscomment(i) = istrue;
    end
    fhasdots = any(spfs.strncmp('...', 3), 2);
    %merge lines that are followed by ... (linebreaks make that the lines are moved back)
    
    % merge lines that cover more than one line (only possible with
    % uneven square brackets [] or curly brackets {})
    % count brackets
    lbracks = sum(spfs.strcmp('[') | spfs.strcmp('{'), 2);
    rbracks = sum(spfs.strcmp(']') | spfs.strcmp('}'), 2);
    lbracks(fiscomment) = 0;
    rbracks(fiscomment) = 0;
    %cumsum to get poplevel of brackets
    brlevel = cumsum(lbracks - rbracks);
    if ~isempty(brlevel) && brlevel(end) > 0
        %problem the brackets are not matching
        n = find(brlevel == 0, 1, 'last');
        if ~isempty(n)
            fprintf('error in line %d:\n%s', n, mfile{n});
        end
        error('format_matlab:brackets', 'Cannot format the text as the brackets {} [] seem not matching');
    end
    lb = sprintf('\n'); %#ok<SPRINTFN>
    if any(fhasdots) || any(fiscomment) || any(brlevel > 0)
        fssndx = true(size(fss));
        for i = length(brlevel): -1:2
            if fiscomment(i) && fiscomment(i - 1)
                if isempty(fss{i - 1})
                    fss{i - 1} = {sprintf('\n%s', sprintf('%s', fss{i}{:}))};
                else
                    fss{i - 1}{end} = sprintf('%s\n%s', fss{i - 1}{end}, sprintf('%s', fss{i}{:}));
                end
                fssndx(i) = false;
            elseif fhasdots(i - 1)
                fss{i - 1} = [fss{i - 1} {lb} fss{i}];
                fssndx(i) = false;
            elseif brlevel(i - 1) > 0
                fss{i - 1} = [fss{i - 1} {lb} fss{i}];
                fssndx(i) = false;
            end
        end
        fss = fss(fssndx);
        spfs = sparse_cell(fss);
    end
    if ischar(mfile)
        fss = fss{1};
    end
end



function [spfs, fs, changed] = exception_anonymous_fun(settings, changed, spfs, fs)
    if settings.exception_anonymous_fun
        [has_anonym_i, has_anonym_j] = find(spfs.strcmp('@'));
        if ~isempty(has_anonym_i)
            
            uhas_anonym_i = unique(has_anonym_i);
            for i = 1:length(uhas_anonym_i)
                fs1 = fs{uhas_anonym_i(i)};
                isanonym = zeros(size(fs1));
                n1 = length(fs1);
                changedline = false;
                fstart = has_anonym_j(uhas_anonym_i(i) == has_anonym_i);
                fspace = strcmp(fs1, ' ');
                lbrack1 = strcmp(fs1, '{') | strcmp(fs1, '[');
                lbracket = strcmp(fs1, '(') | lbrack1;
                rbracket = strcmp(fs1, ')') | strcmp(fs1, '}') | strcmp(fs1, ']');
                lbrack1 = [lbrack1(2:end) false];
                poplevels = cumsum(lbracket - [false rbracket(1:end - 1)]);
                stopsign = rbracket | lbrack1 | lbracket | strcmp(fs1, ',') | strcmp(fs1, ';');
                fend = zeros(size(fstart));
                fstart2 = fend;
                for k = 1:length(fstart)
                    isstartlevel = poplevels <= poplevels(fstart(k));
                    f2 = fstart(k) + 1;
                    while ~lbracket(f2)
                        f2 = f2 + 1;
                    end
                    while ~rbracket(f2)
                        f2 = f2 + 1;
                    end
                    f2 = f2 + 1;
                    while fspace(f2)
                        f2 = f2 + 1;
                    end
                    fstart2(k) = f2;
                    fstop = find(isstartlevel & stopsign);
                    
                    f = find(fstop >= f2);
                    if isempty(f)
                        fend(k) = length(fs1) + 1;
                    else
                        fend(k) = fstop(f(1));
                    end
                    isanonym(fstart(k):fend(k) - 1) = 1;
                end
                fstart = find(diff([0, isanonym 0]) == 1);
                fend = find(diff([0 isanonym, 0]) == -1);
                for k = length(fstart): -1:1
                    s = sprintf('%s', fs1{fstart(k):fend(k) - 1});
                    if settings.remove_space_anonymous_fun && any(s == ' ')
                        fbrack1 = s == '[' | s == '{';
                        fbrack2 = s == ']' | s == '}';
                        popbr = cumsum(double(fbrack1) - double(fbrack2));
                        %no removeing of spaces if [] or {} are in the
                        %function, keep space before ...
                        changed = true;
                        fspace = s == ' ' & popbr == 0;
                        fbeforedots = [strfind(s, '...'), strfind(s, '@')] - 1;
                        fbeforedots(fbeforedots < 1) = [];
                        fspace(fbeforedots) = false;
                        s(fspace) = [];
                    end
                    changedline = true;
                    fs1{fstart(k)} = s;
                    fs1(fstart(k) + 1:fend(k) - 1) = [];
                end
                if changedline
                    spfs(uhas_anonym_i(i), 1:n1) = [fs1, repmat({''}, 1, n1 - length(fs1))];
                    fs{uhas_anonym_i(i)} = fs1;
                end
            end
            spfs = spfs.cleanup;
        end
    end
end

function sett = format_matlab_dlg(sett, meta_info)
    
    ud.sett = [];
    settfields = fieldnames(sett);
    if length(settfields) ~= size(meta_info, 1)
        error('format_matlab:gui', 'Meta info should be the same size');
    end
    [~, ndx] = ismember(settfields, meta_info(:, 1));
    ud.meta_info = meta_info(ndx, :);
    data = [ud.meta_info(:, 2), struct2cell(sett)];
    for i = 1:size(data, 1)
        if iscell(data{i, 2})
            data{i, 2} = strtrim(sprintf('%s ', data{i, 2}{:}));
        end
    end
    
    hfig = figure('Color', [0.914 0.914 0.914], 'PaperUnits', 'inches', 'Units', 'characters', 'Position', [135.8 52.0769230769231 112 32.3076923076923], 'IntegerHandle', 'off', 'MenuBar', 'none', 'Name', 'Settings format_matlab', 'NumberTitle', 'off', 'HandleVisibility', 'callback', 'Tag', 'figure1', 'Resize', 'off', 'PaperPosition', get(0, 'defaultfigurePaperPosition'), 'PaperSize', [8.5 11], 'userdata', ud, 'CreateFcn', @(h, evnt)movegui(h, 'center'));
    
    
    uitable( 'Parent', hfig, 'Units', 'characters', 'FontUnits', get(0, 'defaultuitableFontUnits'), 'BackgroundColor', get(0, 'defaultuitableBackgroundColor'), 'ColumnName', {'Description', 'Value'}, 'ColumnWidth', { 400 130 }, 'ColumnEditable', [false true], 'Data', data, 'RowName', [], 'Position', [0.5 5 111.5 27.5], 'ColumnFormat', { [] [] }, 'CellEditCallback', blanks(0), 'CellSelectionCallback', blanks(0), 'Children', [], 'Tooltip', blanks(0), 'ButtonDownFcn', blanks(0), 'DeleteFcn', blanks(0), 'Tag', 'settingstable', 'UserData', []);
    
    
    uicontrol( 'Parent', hfig, 'Units', 'characters', 'FontUnits', get(0, 'defaultuicontrolFontUnits'), 'String', 'OK', 'Style', get(0, 'defaultuicontrolStyle'), 'Callback', @okbutton, 'Position', [14.2 1.15384615384615 13.8 1.69230769230769], 'Children', [], 'Tag', 'okbutton' );
    
    
    uicontrol( 'Parent', hfig, 'Units', 'characters', 'FontUnits', get(0, 'defaultuicontrolFontUnits'), 'String', 'Cancel', 'Style', get(0, 'defaultuicontrolStyle'), 'Position', [40 1.15384615384615 13.8 1.69230769230769], 'Callback', @cancelbutton, 'Children', [], 'Tag', 'cancelbutton' );
    
    uicontrol( 'Parent', hfig, 'Units', 'characters', 'FontUnits', get(0, 'defaultuicontrolFontUnits'), 'String', 'Help', 'Style', get(0, 'defaultuicontrolStyle'), 'Position', [65.8 1.15384615384615 13.8 1.69230769230769], 'Children', [], 'Callback', @helpbutton, 'Tag', 'pushbutton3' );
    
    
    uiwait(hfig)
    if ishandle(hfig)
        ud = get(hfig, 'userdata');
        sett = ud.sett;
        close(hfig)
    end
    
end

function okbutton(hobj, ~)
    hfig = get(hobj, 'parent');
    ud = get(hfig, 'userdata');
    h = findobj(hfig, 'tag', 'settingstable');
    data = get(h, 'data');
    for i = 1:size(data, 1)
        if ischar(data{i, 2})
            ud.sett.(ud.meta_info{i, 1}) = regexp(data{i, 2}, ' ', 'split');
        else
            ud.sett.(ud.meta_info{i, 1}) = data{i, 2};
        end
    end
    set(hfig, 'userdata', ud);
    drawnow();
    uiresume(hfig)
end

function cancelbutton(hobj, ~)
    hfig = get(hobj, 'parent');
    uiresume(hfig)
end

function helpbutton(~, ~)
    doc('format_matlab')
end

function addformattershortcut(checkcurrent)
    if nargin == 0
        checkcurrent = true;
    end
    try
        fc = com.mathworks.mlwidgets.favoritecommands.FavoriteCommands.getInstance();
        if ~checkcurrent || ~fc.hasCommand('formatter settings', 'FORMAT_MATLAB')
            FormatterCommand = com.mathworks.mlwidgets.favoritecommands.FavoriteCommandProperties();
            FormatterCommand.setLabel('format current page');
            FormatterCommand.setIconName('format_matlab_icon.png');
            FormatterCommand.setIconPath( fileparts(which('format_matlab_icon.png')));
            FormatterCommand.setCategoryLabel('FORMAT_MATLAB'); % use always upper case letters, otherwise I got problems to add furterh favorits 
            FormatterCommand.setCode(sprintf('%%format current page in editor\nformat_matlab(''-c'')'));
            FormatterCommand.setIsOnQuickToolBar(true);
            fc.addCommand(FormatterCommand);
            disp('Added "format current page" to favourites and quick toolbar');
        end
        if ~checkcurrent || ~fc.hasCommand('formatter settings', 'FORMAT_MATLAB')
            SettingsCommand = com.mathworks.mlwidgets.favoritecommands.FavoriteCommandProperties();
            SettingsCommand.setLabel('formatter settings');
            SettingsCommand.setIconName('format_matlab_settings.png');
            SettingsCommand.setIconPath( fileparts(which('format_matlab_settings.png')));
            SettingsCommand.setCategoryLabel('FORMAT_MATLAB'); % use always upper case letters, otherwise I got problems to add furterh favorits 
            SettingsCommand.setCode(sprintf('%%Settings for formatter\nformat_matlab(''-s'')'));
            SettingsCommand.setIsOnQuickToolBar(false);

            % add the command to the favorite commands (the category is automatically created if it doesn't exist)
            fc.addCommand(SettingsCommand);
            disp('Added "format settings" to favourites');
        end
    catch err
        if strcmp(err.identifier, 'MATLAB:undefinedVarOrClass')
            disp('Can not add shortcut in MATLAB versions < R2018a');
        end
    end
end



