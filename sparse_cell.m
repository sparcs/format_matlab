classdef sparse_cell
    %implements an efficient 2D sparse cell of strings
    %designed to access non-uniform sized 2D cells of strings
    %especially fast searching (strcmp) if there are many double elements
    %
    %
    
    properties (SetAccess = public)
        uelems = []; %unique elements
        uelemnr = []; %unique integers key of each uelems
        changed = [];
        elems = []; %sparse matrix of all elements with the unique keys
    end
    
    methods (Static)
        function r = makerect(c)
            %make a rectangular cell array of a non-uniform cell array
            %can be handy for testing
            lens = cellfun(@numel, c);
            maxlen = max(lens);
            r = cell(length(c), maxlen);
            r(:) = {''};
            for i = 1:size(r, 1)
                r(i, 1:lens(i)) = c{i}(:);
            end
        end
    end
    
    
    methods (Access = public, Hidden = false)
        
        function obj = subsdel(obj, s)
            siz = size(obj.elems);
            colonrow = ischar(s.subs{1}) && strcmp(s.subs{1}, ':');
            if numel(s.subs) > 1
                coloncol = ischar(s.subs{2}) && strcmp(s.subs{2}, ':');
            else
                coloncol = false;
            end
            if ~coloncol && ~colonrow && ~any(siz == 1)
                error('spars_cell:delete', 'A null assignment can have only one non-colon index.');
            end
            deletedelems = [];
            if numel(s.subs) == 1
                if colonrow
                    obj.elems(:) = [];
                    obj.uelems = {};
                    obj.uelemnr = [];
                    obj.changed = [];
                else
                    deletedelems = obj.elems(s.subs{1});
                    obj.elems(s.subs{1}) = [];
                end
            elseif numel(s.subs) == 2
                if colonrow && coloncol
                    obj.elems = [];
                    obj.uelems = {};
                    obj.uelemnr = [];
                    obj.changed = [];
                elseif colonrow
                    deletedelems = obj.elems(:, s.subs{2});
                    obj.elems(:, s.subs{2}) = [];
                else
                    deletedelems = obj.elems(s.subs{1}, :);
                    obj.elems(s.subs{1}, :) = [];
                end
            end
            deletedelems = unique(deletedelems(:));
            for i = 1:length(deletedelems)
                ndx = obj.uelemnr == deletedelems(i);
                obj.changed(ndx) = true;
            end
            %             for i = 1:length(deletedelems)
            %                 if ~any(deletedelems(i) == obj.elems)
            %                     ndx = obj.uelemnr == deletedelems(i);
            %                     obj.uelemnr(ndx) = [];
            %                     obj.uelems(ndx) = [];
            %                     obj.changed(ndx) = [];
            %                 end
            %             end
            
        end
        
        
        function n = numArgumentsFromSubscript(obj, s, varargin)
            %is called internally when referencing (needed for curly
            %brackets)
            switch s.type
                case '()'
                    n = 1;
                case '{}'
                    nrow = 1;
                    if numel(s.subs) > 0
                        if ischar(s.subs{1}) && strcmp(s.subs{1}, ':')
                            if numel(s.subs) == 1
                                nrow = numel(obj.elems);
                            else
                                nrow = size(obj.elems, 1);
                            end
                        else
                            nrow = numel(s.subs{1});
                        end
                    end
                    if numel(s.subs) > 1
                        if ischar(s.subs{2}) && strcmp(s.subs{2}, ':')
                            ncol = size(obj.elems, 2);
                        else
                            ncol = numel(s.subs{2});
                        end
                    else
                        ncol = 1;
                    end
                    n = nrow * ncol;
                otherwise
                    n = 1; %builtin('numArgumentsFromSubscript', obj, s, varargin{:});
            end
        end
        
        function obj = subsasgn(obj, s, varargin)
            %try to make sparse_cell behave as cell array of strings
            function asgn_single(index, newdat, indexnewdat)
                if numel(newdat) > 1
                    newdat = newdat{indexnewdat{:}};
                elseif iscell(newdat)
                    newdat = newdat{1};
                end
                try
                    oldelemnr = obj.elems(index{:});
                catch
                    oldelemnr = 0;
                end
                if isempty(newdat)
                    elemnr = 0;
                else
                    elemnr = obj.uelemnr(strcmp(obj.uelems, newdat));
                    if isempty(elemnr)
                        elemnr = max(obj.uelemnr) + 1;
                        obj.uelemnr(end + 1) = elemnr;
                        obj.uelems{end + 1} = newdat;
                        obj.changed(end + 1) = false;
                    end
                end
                if elemnr ~= oldelemnr
                    obj.elems(index{:}) = elemnr;
                    %if ~any(oldelemnr == obj.elems)
                    ndx = obj.uelemnr == oldelemnr;
                    obj.changed(ndx) = true;
                    %obj.uelemnr(ndx) = [];
                    %obj.uelems(ndx) = [];
                    %end
                end
            end
            if length(s) ~= 1 || ~any(strcmp(s.type, {'{}', '()'}))
                obj = builtin('subsasgn', obj, s, varargin{:});
            else
                siz = size(obj.elems);
                if numel(s.subs) > 0
                    if ischar(s.subs{1}) && strcmp(s.subs{1}, ':')
                        if numel(s.subs) == 1
                            irow = 1:prod(siz);
                        else
                            irow = 1:siz(1);
                        end
                    else
                        irow = s.subs{1};
                    end
                end
                if numel(s.subs) > 1
                    if ischar(s.subs{2}) && strcmp(s.subs{2}, ':')
                        icol = 1:siz(2);
                    else
                        icol = s.subs{2};
                    end
                else
                    icol = [];
                end
                switch s.type
                    case '()'
                        newdata = varargin{1};
                    case '{}'
                        newdata = varargin;
                end
                if isempty(newdata)
                    obj = obj.subsdel(s);
                    return;
                end
                if isempty(icol)
                    for i = 1:length(irow)
                        asgn_single({irow(i)}, newdata, {i})
                    end
                else
                    for i = 1:length(irow)
                        for j = 1:length(icol)
                            asgn_single({irow(i), icol(j)}, newdata, {i, j})
                        end
                    end
                end
            end
        end
        
        function varargout = subsref(obj, s)
            %try to make sparse_cell behave like a cell of strings
            function res1 = getelemstr(elemnr)
                if elemnr == 0
                    res1 = '';
                else
                    res1 = obj.uelems{obj.uelemnr == elemnr};
                end
            end
            
            if length(s) ~= 1 || ~any(strcmp(s.type, {'{}', '()'}))
                [varargout{1:nargout}] = builtin('subsref', obj, s);
            else
                siz = size(obj.elems);
                if numel(s.subs) > 0
                    if ischar(s.subs{1}) && strcmp(s.subs{1}, ':')
                        if numel(s.subs) == 1
                            irow = 1:prod(siz);
                        else
                            irow = 1:siz(1);
                        end
                    else
                        irow = s.subs{1};
                    end
                end
                if numel(s.subs) > 1
                    if ischar(s.subs{2}) && strcmp(s.subs{2}, ':')
                        icol = 1:siz(2);
                    else
                        icol = s.subs{2};
                    end
                else
                    icol = [];
                end
                res = cell(length(irow), max(1, length(icol)));
                for i = 1:length(irow)
                    if isempty(icol)
                        res{i} = getelemstr(obj.elems(irow(i)));
                    else
                        for j = 1:length(icol)
                            res{i, j} = getelemstr(obj.elems(irow(i), icol(j)));
                        end
                    end
                end
                switch s.type
                    case '()'
                        varargout = {res};
                    case '{}'
                        varargout = res;
                end
            end
        end
    end
    
    methods (Access = public, Hidden = false)
        
        function obj = sparse_cell(cellvar)
            c_nempty=[];
            try
            if iscell(cellvar{1})
                %more complex for non-uniform cell arrays, is much faster
                %than converting first to full square cell array.
                lens = cellfun(@numel, cellvar);
                maxlen = max(lens);
                siz = [length(cellvar) maxlen];
                cumlen = [0; cumsum(lens)];
                c_nempty = cell(sum(lens), 1);
                ndx_i = zeros(size(c_nempty));
                ndx_j = zeros(size(c_nempty));
                for i = 1:size(cellvar, 1)
                    c_nempty(cumlen(i) + 1:cumlen(i + 1)) = cellvar{i}(:);
                    ndx_i(cumlen(i) + 1:cumlen(i + 1)) = zeros(1, lens(i)) + i;
                    ndx_j(cumlen(i) + 1:cumlen(i + 1)) = 1:lens(i);
                end
                ndx_nempty = sub2ind(siz, ndx_i, ndx_j);
                %if needed remove other empty cells
                ndx = ~strcmp(c_nempty, '');
                ndx_nempty = ndx_nempty(ndx);
                c_nempty = c_nempty(ndx);
            else
                %cellvar is already a square cell array 
                siz = size(cellvar);
                c_nempty = cellvar(:);
                ndx_nempty = find(~strcmp(c_nempty, ''));
                c_nempty = c_nempty(ndx_nempty);
            end
            %sort can potentially be slow. Therefore we avoid sorting
            %many empty elements!
            [b, ndx] = sort(c_nempty);
            fndx = ndx_nempty(ndx);
            %b = b(not_empty);
            % fndx = ndx(not_empty);
            ddiff = ~strcmp(b(1:end - 1), b(2:end));
            sameelems = cumsum([1; ddiff(1:end)]);
            obj.uelems = b([ddiff; true]);
            obj.changed = false(size(obj.uelems));
            obj.uelemnr = transpose(1:length(obj.uelems));
            elems1 = zeros(siz);
            elems1(fndx) = sameelems;
            obj.elems = sparse(elems1);
            catch
                %handle the special cases such as all empty or only one
                %elem (that cannot be sorted)
              if isempty(cellvar)
                  obj.elems = sparse(0,0);
                  return;
              elseif iscell(cellvar{1})
                  lens = cellfun(@numel, cellvar);
                  siz=[length(cellvar),max(lens)];
                  obj.elems=sparse(siz(1),siz(2));
              end
            end
        end
        
        
        function ndx = uelem2ndx(obj, iuelem)
            %index of uelem to index of elems
            elemnr = obj.uelemnr(iuelem);
            if ~isempty(elemnr)
                if length(elemnr) > 10
                    ndx = sparse(ismember(obj.elems, elemnr));
                else
                    ndx = obj.elems == elemnr(1);
                    for i = 2:length(elemnr)
                        ndx = ndx | obj.elems == elemnr(i);
                    end
                end
            else
                siz = size(obj.elems);
                ndx = sparse([], [], logical([]), siz(1), siz(2));
            end
        end
   
        function ndx = ismemberndx(obj, elem)
            if ischar(elem)
                elem = {elem};
            end
            ndx=zeros(size(obj.elems));
            for i = 1:length(elem)
                iuelem = strcmp(elem{i}, obj.uelems);
                ndx1 = obj.uelem2ndx(iuelem);
                ndx(ndx1)=i;
            end
            ndx=sparse(ndx);
        end
        
        function ndx = ismember(obj, elem)
            %ismember is not so efficient for low number of elements
            if ischar(elem)
                elem = {elem};
            end
            if isempty(elem)
                iuelem = false(size(obj.uelems));
            elseif length(elem) < 10
                iuelem = strcmp(elem{1}, obj.uelems);
                for i = 2:length(elem)
                    iuelem = iuelem | strcmp(elem{i}, obj.uelems);
                end
            else
                iuelem = ismember(obj.uelems, elem);
            end
            ndx = obj.uelem2ndx(iuelem);
        end
        
        function ndx = strcmp(obj, elem)
            [obj, elem] = exchangeargs(obj, elem);
            if iscell(elem)
                ndx = strcmp(elem, obj.full);
                return;
            end
            iuelem = strcmp(elem, obj.uelems);
            ndx = obj.uelem2ndx(iuelem);
        end
        
        function ndx = strcmpi(obj, elem)
            [obj, elem] = exchangeargs(obj, elem);
            if iscell(elem)
                ndx = strcmpi(elem, obj.full);
                return;
            end
            iuelem = strcmpi(elem, obj.uelems);
            ndx = obj.uelem2ndx(iuelem);
        end
        
        function ndx = strncmp(obj, elem, len)
            [obj, elem] = exchangeargs(obj, elem);
            if iscell(elem)
                ndx = strncmp(elem, obj.full, len);
                return;
            end
            iuelem = strncmp(elem, obj.uelems, len);
            ndx = obj.uelem2ndx(iuelem);
        end
        
        function ndx = any_regexp_match(obj, regexpkey)
            if isempty(obj.uelems)
                ndx=obj.uelem2ndx([]);
                return;
            end
            c=regexp(obj.uelems,regexpkey);
            iuelem=~cellfun('isempty',c);
            %iuelem = strncmpi(elem, obj.uelems, len);
            ndx = obj.uelem2ndx(iuelem);
        end
        
        function ndx = strncmpi(obj, elem, len)
            [obj, elem] = exchangeargs(obj, elem);
            if iscell(elem)
                ndx = strncmpi(elem, obj.full, len);
                return;
            end
            iuelem = strncmpi(elem, obj.uelems, len);
            ndx = obj.uelem2ndx(iuelem);
        end
        
        
        function fs = sparse2cell(obj, ndx)
            %if no index is used, this function is much slower than full!
            if nargin == 1
                ndx = 1:size(obj.elems, 1);
            end
            if islogical(ndx) && length(ndx) == size(obj.elems, 1)
                ndx = find(ndx);
            end
            fs = cell(numel(ndx), 1);
            %fliplr as reverse directive not available in older MATLAB
            %versions
            lens = full(sum(cumsum(fliplr(obj.elems), 2) ~= 0, 2));
            for i = 1:numel(ndx)
                fs1 = cell(1, lens(ndx(i)));
                for j = 1:lens(ndx(i))
                    elemnr = obj.elems(ndx(i), j);
                    if elemnr == 0
                        fs1{j} = '';
                    else
                        fs1{j} = obj.uelems{obj.uelemnr == elemnr};
                    end
                end
                fs{i} = fs1;
            end
        end
        
        function obj = cleanup(obj)
            ndx = find(obj.changed);
            for i = length(ndx): - 1:1
                if ~any(obj.elems == obj.uelemnr(ndx(i)))
                    obj.uelems(ndx(i)) = [];
                    obj.uelemnr(ndx(i)) = [];
                    %obj.changed(ndx(i))=[];
                end
            end
            obj.changed = false(size(obj.uelems));
        end
        
        function c = full(obj)
            c = cell(size(obj.elems));
            c(:) = {''};
            for i = 1:length(obj.uelemnr)
                c(obj.elems == obj.uelemnr(i)) = obj.uelems(i);
            end
        end
        
    end
    
end
function [obj, elem] = exchangeargs(obj, elem)
    if isa(elem, 'sparse_cell')
        h = obj;
        obj = elem;
        elem = h;
    end
end
