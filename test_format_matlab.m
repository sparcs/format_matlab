function test_format_matlab(rerun)
    if nargin == 0
        rerun = false;
    end
    oldsett = format_matlab('-s'); %get default settings as structure
    sett=oldsett;
    sett.exception_anonymous_fun = true;
    sett.remove_space_anonymous_fun = true;
    sett.remove_double_space = true;
    sett.linebreak_after_semicolon = true;
    sett.remove_unneeded_spaces = false;
    sett.addspace_before_comment = true;
    sett.addspace_before = {'~'};
    sett.addspace_around_no_unary = {'+' '-'};
    sett.addspace_after = {',' ';'};
    sett.addspace_around = {'=', '/', '\', '*', '.*', './', '.\', '|', '||', '&', '&&', '>', '<', '>=', '<=', '==', '~='};
    sett.remove_semicolons = true;
    sett.remove_semicolons_keywords =  {'end', 'function', 'for', 'parfor', 'switch', 'case', 'if', 'while', 'catch', 'else', 'elseif'};
    sett.replace_tabs_w_space = true;
    sett.update_indent = true;
    sett.indent_keywords = {};
    sett.indent_line_extension = 4;
    sett.FunctionIndentingFormat = 'AllFunctionIndent';
    sett.add_end_to_oldstylefunctions = true;
    sett.addspace_around_no_unary = {'+' '-'};
    sett2 = sett;
    sett2.exception_anonymous_fun = true;
    sett2.remove_space_anonymous_fun = true;
    sett2.remove_double_space = false;
    sett2.linebreak_after_semicolon = true;
    sett2.remove_unneeded_spaces = true;
    sett2.addspace_before_comment = false;
    sett2.addspace_before = {};
    sett2.addspace_around_no_unary = {};
    sett2.addspace_after = {',' ';'};
    sett2.addspace_around = {};
    sett2.remove_semicolons = true;
    sett2.remove_semicolons_keywords =  {'end', 'function', 'for', 'parfor', 'switch', 'case', 'if', 'while', 'catch', 'else', 'elseif'};
    sett2.replace_tabs_w_space = true;
    sett2.update_indent = true;
    sett2.indent_keywords = {};
    sett2.indent_line_extension = 0;
    sett2.FunctionIndentingFormat = 'ClassicFunctionIndent';
    sett2.add_end_to_oldstylefunctions = true;
    sett2.addspace_around_no_unary = {};
    
%if false
    %here the tests start
    assert_format(sett2,{'plot; title ''Distribution richness'';'},{'plot;','title ''Distribution richness'';'})
    assert_format(sett,{'h=@(x)[x x]+2'},{'h = @(x)[x x]+2'})
    assert_format(sett, {'{@(b)a []}'}, {'{@(b)a []}'})
    assert_format(sett,{'if isempty','chckxywp;','else [x] = chckxywp;','end'},{'if isempty',nspace('chckxywp;',4),'else [x] = chckxywp;','end'})
    assert_format(sett,{'if a','b;','else if c','d;','end','end'},{'if a',nspace('b;',4),'else if c',nspace('d;',8),nspace('end',4),'end'});

    assert_format(sett, {'dbstop if a1;a'}, {'dbstop if a1;', 'a'})
    assert_format(sett, {'{@(x)sin(x) @(x)cos(2*x)}'}, {'{@(x)sin(x) @(x)cos(2*x)}'})
    assert_format(sett2, {'pb = GUIPushButton( @() 1 ...', ...
        ', ''indexChanged'');'}, {'pb=GUIPushButton(@()1 ...', ', ''indexChanged'');'});
    assert_format(sett, {'f = @(x)bowlpeakfun(x,a,b,c)'}, {'f = @(x)bowlpeakfun(x,a,b,c)'});
    assert_format(sett, {'fhan = @(x,p)fhan(x,p,mode);end'}, {'fhan = @(x,p)fhan(x,p,mode);', 'end'});
    assert_format(sett, {'a = {@(o,e)a.a}; end'}, {'a = {@(o,e)a.a};', 'end'});
    assert_format(sett, 'cleanUpObject=onCleanup(@()cellfun(@(x)x(), {@()zipFile.close,@()delete(cleanUpUrl)}))*1;', ...
        'cleanUpObject = onCleanup(@()cellfun(@(x)x(),{@()zipFile.close,@()delete(cleanUpUrl)})) * 1;')
    assert_format(sett, 'if 1\na=[1 2 3];\nend;', 'if 1\n    a = [1 2 3];\nend');
    assert_format(sett, {'try a;catch end;a'}, {'try a;', 'catch end;', 'a'})
    assert_format(sett, {'methods=1;a'}, {'methods = 1;', 'a'})
    try
        assert_format(sett2, {'{1 [--4 4}'}, {'{1 --4 4}'})
    catch err
        if ~strcmp(err.identifier, 'format_matlab:brackets')
            rethrow(err);
        else
            disp(err.message);
        end
    end
    assert_format(sett, {'            st(x) = '''';str = mat2str(st);'}, {'st(x) = '''';', 'str = mat2str(st);'});
    assert_format(sett, {'["a"   "b" -100]'}, {'["a" "b" -100]'})
    assert_format(sett, {'"a"+"b"'}, {'"a" + "b"'})
    assert_format(sett, {'"a"=="b"&&"c">"b"'}, {'"a" == "b" && "c" > "b"'})
    assert_format(sett, {'a==a  +b'}, {'a == a + b'})
    format_matlab('-setdefault',sett);
    assert_format(struct('remove_unneeded_spaces', true), {'a==a  +b'}, {'a == a + b'})
    assert_format(struct('exception_anonymous_fun', false), {'@(x)x+1'}, {'@(x)x + 1'})
    assert_format(sett, {'a+b', '%{ ', '   b+c', '%}  ', 'b*c'}, {'a + b', '%{ ', '   b+c', '%}  ', 'b * c'})
    assert_format(sett, {'a+b', '%#format<off>', 'b+c', '%#format<on>', 'b*c'}, {'a + b', '%#format<off>', 'b+c', '%#format<on>', 'b * c'})
    assert_format(sett, {'a(3,4:5)=[3,1]'}, {'a(3, 4:5) = [3, 1]'})
    assert_format(sett, {'while i>-1,+a;end;'}, {'while i > -1', nspace('+a;', 4), 'end'})
    assert_format(sett, {'+(-[-.1])+r(+1)+-1.'}, {'+(-[-.1]) + r(+1) + -1.'})
    disp('The next warning is ok:');
    assert_format(sett, {'if-i>-b'}, {'if -i > -b'})
    [~, lastid] = lastwarn;
    lastwarn('');
    assert(strcmp(lastid, 'format_matlab:nomatchingend'), 'No warning for missing end')
    assert_format(sett, {'a=[@(x) minus(x,1), ]'}, {'a = [@(x)minus(x,1), ]'})
    assert_format(sett, {'--.15'}, {'--.15'})
    assert_format(sett, {'[1+4 a  +-+-.3]'}, {'[1 + 4 a +-+-.3]'})
    assert_format(struct('remove_unneeded_spaces', true), {'[1+4 a  +-+-.3]'}, {'[1 + 4 a +-+-.3]'})
    assert_format(sett, {'{1 --4 {a+  +b} 4}'}, {'{1 --4 {a + +b} 4}'})
    assert_format(sett2, {'{1 --4 4}'}, {'{1 --4 4}'})
    assert_format(sett, {'a -b+c   b'}, {'a -b+c b'})
    assert_format(struct('remove_unneeded_spaces', true), {'a -b+c   b'}, {'a -b+c b'})
    assert_format(sett, {'a +b+c   b'}, {'a +b+c b'})
    assert_format(sett, {'a =b+c   *b'}, {'a = b + c * b'})
    s1 = {'function     a,if  b,c;end,,function  b,  a;b; c;'}';
    s2 = {'function a', 'if b', nspace('c;', 4), 'end', '', 'function b', 'a;', 'b;', 'c;'};
    assert_format(sett2, s1, s2);
    sett2.FunctionIndentingFormat = 'MixedFunctionIndent';
    s1 = {'function     a,if  b,c;end,,function  b,  a;b; c;'}';
    s2 = {'function a', 'if b', nspace('c;', 4), 'end', '', 'function b', 'a;', 'b;', 'c;'};
    assert_format(sett2, s1, s2);
    s1 = {'function     a,if  b,c;end,end,,function  b,  a;b; c;end'}';
    s2 = {'function a', nspace('if b', 4), nspace('c;', 8), nspace('end', 4), 'end', '', 'function b', nspace('a;', 4), nspace('b;', 4), nspace('c;', 4), 'end'};
    assert_format(sett2, s1, s2);
 
    assert_format(sett, s1, s2);

    s1 = {'while (1),', '     a = [0, 1];  ', '     a(1) = 2 * [a(0)];', 'break', 'end;'}';
    s2 = {'while (1)', nspace('a = [0, 1];', 4), nspace('a(1) = 2 * [a(0)];', 4), nspace('break', 4), 'end'}';

    assert_format(sett, s1, s2);
    assert_format(sett, {'[@(x)sin(x+1),4+6,@(x)cos(2*x)]'}, {'[@(x)sin(x+1), 4 + 6, @(x)cos(2*x)]'});
    assert_format(sett, {'[@(x)sin(x + 1),4+6,@(x) cos(2 * x)]'}, {'[@(x)sin(x+1), 4 + 6, @(x)cos(2*x)]'});

    assert_format(sett, {'-x'' + +1+x''''+2*x''''''*1'}, {'-x'' + +1 + x'''' + 2 * x'''''' * 1'});
    assert_format(sett2, {'-x'' + +1+x''''+2*x''''''*1'}, {'-x''++1+x''''+2*x''''''*1'});
    assert_format(sett, {'a=a+1*b''/c''++1'}, {'a = a + 1 * b'' / c'' + +1'})
    assert_format(sett2, {'a = a + 1 * b'' / c'' + + 1 %comm'}, {'a=a+1*b''/c''++1%comm'})
    sett.addspace_around_no_unary = {};
    assert_format(sett, {'a=a+1*b''/c''-s(''a+n'')'}, {'a = a+1 * b'' / c''-s(''a+n'')'})
    assert_format(sett2, {'    a=a+     1 * b'' / c'' - s(''a + n'')'}, {'a=a+1*b''/c''-s(''a + n'')'})
    sett.addspace_around_no_unary = {'+' '-'};
    s1 = {'switch x', 'case 1', 'if any(z == -[-1, -2, -3, -4])', 'ifmyvariablenamecontainsif=z(1:end);', 'end', 'end'};
    s2 = {'switch x', nspace('case 1', 4), nspace('if any(z == -[-1, -2, -3, -4])', 8), nspace('ifmyvariablenamecontainsif = z(1:end);', 12), nspace('end', 8), 'end'};
    assert_format(sett, s1, s2);
    s2 = {'switch x', nspace('case 1', 4), nspace('if any(z==-[-1, -2, -3, -4])', 8), nspace('ifmyvariablenamecontainsif=z(1:end);', 12), nspace('end', 8), 'end'};
    assert_format(sett2, s1, s2);
    s1 = {'   if true||... % aaa', 'true||...a+s*d', 'false % //end', '   end'};
    s2 = {'if true || ... % aaa', nspace('true || ...a+s*d', 8), nspace('false % //end', 8), 'end'};
    assert_format(sett, s1, s2);

    s1 = {'a={'''',... % hello', '1,4+... %hello2', '     2} % hello3'};
    s2 = {'a = {'''', ... % hello', nspace('1, 4 + ... %hello2', 4), nspace('2} % hello3', 4)};
    assert_format(sett, s1, s2);
    s1 = {sprintf('\ta = [1..*2, 3 ... %% 111'), '.*2, ... % 222', '123, 4 -  ...', '5] % 333'};
    s2 = {'a = [1. .* 2, 3 ... % 111', nspace('.* 2, ... % 222', 4), nspace('123, 4 - ...', 4), nspace('5] % 333', 4)};
    assert_format(sett, s1, s2);
    s1 = {'if i==1,a=3;b=-b;end%comm'};
    s2 = {'if i == 1', nspace('a = 3;', 4), nspace('b = -b;', 4), 'end %comm'};
    assert_format(sett, s1, s2);

    s1 = {'a={1 ''x''    %this is weird but correct', '3   6', '{4,''x''} 4', '5 6};'};
    s2 = {'a = {1 ''x'' %this is weird but correct', '    3 6', '    {4, ''x''} 4', '    5 6};'};
    assert_format(sett, s1, s2);
    assert_format(sett, {'sprintf if end else for   while %a should not indent', 'a'}, {'sprintf if end else for while %a should not indent', 'a'});
    format_matlab('-setdefault', oldsett);
    thedir = fileparts(mfilename('fullpath'));
    filenam = fullfile(thedir, 'errorfiles.mat');
    if rerun
        %run tests on all matlab files I have (takes really long) saves
        %files with error
        %change folder names if needed
        dirs = {'c:\d\alg\mat lab', 'C:\Program Files\MATLAB\R2011b\toolbox', 'C:\Program Files\MATLAB\R2019b\toolbox'};
        errorfiles = {};
        olddir = cd;
        for i = 1:length(dirs)
            cd(dirs{i});
            s = format_matlab('-t', '**\*.m');
            errorfiles = [errorfiles; s]; %#ok<AGROW>
        end
        cd(olddir);
        save(filenam, 'errorfiles');
    end
    if exist(filenam, 'file')
        load(filenam, 'errorfiles');
        files = strtrim(regexp(errorfiles, '(?<=errors in )[^<]*', 'match', 'once'));
        errorfiles = cell(size(files));
        filendx = true(size(errorfiles));
        for i = 1:length(files)
            efiles = format_matlab('-t', files{i});
            if ~isempty(efiles)
                errorfiles{i} = efiles{1};
            else
                filendx(i) = false;
            end
        end
        errorfiles = errorfiles(filendx);
        save(filenam, 'errorfiles');
        % disp(errorfiles);
    end
    disp('ok')
end

function str = nspace(str, n)
    str = [repmat(' ', 1, n) str];
end
function assert_format(sett, ctext1, ctext2)
    if ischar(ctext1)
        ctext1 = str2cell(sprintf(ctext1));
    end
    if ischar(ctext2)
        ctext2 = str2cell(sprintf(ctext2));
    end
    res = format_matlab(ctext1, sett);
    isok = length(res) == length(ctext2) && all(strcmp(ctext2(:), res));
    fprintf('%s\n', res{:});
    if ~isok
        fprintf(2, '%s\n', res{:});
        fprintf(2, 'should be:\n');
        fprintf(2, '%s\n', ctext2{:});
    end
    assert(isok, 'format_matlab:assert', 'format not working')
end
