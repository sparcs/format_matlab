# format_matlab

format_matlab is a simple fast formatter of MATLAB code. The purpose is to 
make the code easier to read.
It includes a test modus where it is checked
whether there are changes in the warnings that MATLAB gives before and after
formatting to minimize the risk of unwanted changes in the code.

**Key properties:**
   - Add spaces around operators
   - put commands on separate lines
   - remove spaces from anonymous functions for readability (or leave
     unchanged)
   - add "end" to oldstyle functions
   - remove unnecessary semicolons
   
Much effort is taken to keep the code robustly the same. For instance 
removing/adding spaces can sometimes change the meaning of code ([1 -2] vs [1 - 2]. 
Please send me code + settings when you encounter bugs.
   

**Usage:**\
     `format_matlab` - if no arguments are given, the current editor
            page is formatted.\
     `format_matlab afile` - if no output file is given, the new text is only printed\
     `format_matlab fromfile tofile` - changes written to tofile (can be
            the same name)\
     `format_matlab('fromfile','tofile',sett)` - use settings as
            specified in the structure sett (see below)\
     `sett=format_matlab('-s')`; get a structure with the current default
            settings\
     `format_matlab('-s',struct('ignore_anonymous_fun',false))` - change
            the default settings (only the fields that deviate from the
            default settings need to be defined)\
     `format_matlab('-s',[])` reset the default settings to the preset
            ones\
     `format_matlab -d dir` - inplace formatting of all files in
            directory. Note there is no way to revert this. Make backups!\
     `format_matlab -t dir` - test modus formatting of all files in dir.
            If your matlab version supports it, you can use **\\*.m
            to scan subdirectories too. In the test a comparison of the 
            warnings generated with the matlab command "checkcode". 
            The changes in warnings are showed on the screen.
 
   **Settings stucture:**\
     Fields:\
     _ignore_anonymous_fun_ - logical set true if you don't want to
                            format anonymous functions\
     _remove_space_anonymous_fun_- logical set true if you want to
                            remove spaces from anonymous functions\
     _remove_double_space_  - logical, remove double spaces? (except
                            indent)\
     _linebreak_after_semicolon_ - logical, make new lines after ';' and ','
     _remove_other_spaces_  - logical, remove first all uneccesary spaces
                            (run only once with true, otherwise slow!)\
     _addspace_before_comment_- logical, add one space before each
                            comment\
     _addspace_before_        - cell list of opers that should get a
                            space before them (for instance '~')\
     _addspace_around_no_unary_ - cell list of special opers that can be
                            unary or not ('+' and '-'}. Depending on
                            their role they get spaces around or spaces
                            after, note that in somme cases the spaces
                            determine the role [0 -1] or [0 - 1]\
     _addspace_after_       - cell, list of opers that should have space
                            after them (for instance {';'}\
     _addspace_around_      - cell, list of opers that should have spaces
                            at both sides (for instance {'*'})\
     _remove_semicolons_    - logical remove unneccesary semicolons after
                            'end', 'function', 'if' etc\
     _remove_semicolons_keywords_ - cell list of keywords to remove
                            semicolons\
     _replace_tabs_w_space_ - replace tabs with space (necessary for
                            correct indentation)\
     _update_indent_        - logical if true update teh indentation\
     _indent_keywords_      - structure with keywords and indentation rules\
     _indent_line_extension_ - integer number of extra spaces in multi-line statements\
     _FunctionIndentingFormat_ - kind of behaviour with functions (see MATLAB settings:
                            ClassicFunctionIndent or MixedFunctionIndent or AllFunctionIndent\
     _add_end_to_oldstylefunctions_ - logical if set to true all functions get a trailing end
 
 
   **Directives in text:**\
     %#format<off> stop formatting from this line on\
     %#format<on> start formatting again from this line on
 
   **Known issues:**\
     (*) After splitting a line, the warning suppression directives are put
         on the last line. This will often be the wrong line. For instance:
 
         for i=1:n,a=[a i];end; %#ok<AGROW>
 
         becomes:
         for i = 1:n
            a = [a i];
         end %#ok<AGROW>
 
         This will generate 2 warnings: 1 the formerly suppressed warning and a
         warning that the suppression of the last line is no longer needed.
         Solution: move the directive to the correct place(s) afterwards
 
 
     (*) Statements after a if/while/elseif condition delimited only with space are
         not fully supported. The following will be wrongly indended and a warning is given:
         if cond if cond1
            test,end
         end
 
         Solution: add comma or semicolon
         if cond, if cond1
            test,end
         end
 
**Contact**\
   Egbert van Nes, WUR, Egbert.vannes@wur.nl
